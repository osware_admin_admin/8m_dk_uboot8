

cp myir-imx-uboot/tools/mkimage                      ./imx-mkimage/iMX8M/mkimage_uboot
cp myir-imx-uboot/arch/arm/dts/myb-imx8mm-base.dtb   ./imx-mkimage/iMX8M/fsl-imx8mm-ddr4-evk.dtb
cp myir-imx-uboot/spl/u-boot-spl.bin                 ./imx-mkimage/iMX8M/
cp myir-imx-uboot/u-boot-nodtb.bin                   ./imx-mkimage/iMX8M/

# firmware-imx-8.7 
cp firmware-imx-8.7/firmware/ddr/synopsys/ddr4_dmem_1d.bin                     ./imx-mkimage/iMX8M/
cp firmware-imx-8.7/firmware/ddr/synopsys/ddr4_dmem_2d.bin                     ./imx-mkimage/iMX8M/
cp firmware-imx-8.7/firmware/ddr/synopsys/ddr4_imem_1d.bin                     ./imx-mkimage/iMX8M/
cp firmware-imx-8.7/firmware/ddr/synopsys/ddr4_imem_2d.bin                     ./imx-mkimage/iMX8M/

# imx8mm-atf
cp imx-atf/build/imx8mm/release/bl31.bin                                    ./imx-mkimage/iMX8M/

cd imx-mkimage
make SOC=iMX8MM clean
make SOC=iMX8MM flash_ddr4_evk_qspi
