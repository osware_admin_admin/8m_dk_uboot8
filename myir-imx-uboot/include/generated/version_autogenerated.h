#define PLAIN_VERSION "2019.04-g65e4ca0a-dirty"
#define U_BOOT_VERSION "U-Boot " PLAIN_VERSION
#define CC_VERSION_STRING "aarch64-poky-linux-gcc (GCC) 9.2.0"
#define LD_VERSION_STRING "GNU ld (GNU Binutils) 2.32.0.20190204"
